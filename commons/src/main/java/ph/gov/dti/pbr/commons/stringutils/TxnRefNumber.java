/*
 *  Commons String Utility - <one line to give a brief idea of what it does.>
 *  Copyright (C) 2016 Department of Trade and Industry.
 */
package ph.gov.dti.pbr.commons.stringutils;

import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author Marlon Janssen Arao
 * @since 6 October 2016
 * @version 1.0.0
 */
public class TxnRefNumber {
    private static class TxnRefNumberManagerHolder {
        private final static TxnRefNumber INSTANCE =
                new TxnRefNumber();
    }
    
    private TxnRefNumber() { }
    
    public static TxnRefNumber getInstance() {
        return TxnRefNumberManagerHolder.INSTANCE;
    }
    
    /**
     * 
     * @param numericLength the length of the number to generate
     * @return the transaction reference number
     */
    public static String generate(int numericLength) {
        return RandomStringUtils.randomNumeric(numericLength);
    }
}
