/*
 *  GCash Payment Client - <one line to give a brief idea of what it does.>
 *  Copyright (C) 2016 Department of Trade and Industry.
 */
package ph.gov.dti.pbr.payment;

import java.io.Serializable;

/**
 *
 * @author Marlon Janssen Arao
 * @since 5 October 2016
 * @version 1.0.0
 */
public class GcashTxnInfos implements Serializable {
    private String merchantReferenceNumber = null;
    private String merchantTransactionName = null;
    private String mobileNumber = null;
    private String pin = null;
    private float amount = 0.0f;
    
    public GcashTxnInfos() { }

    /**
     * @return The merchant's Transaction Reference Number
     */
    public String getReferenceNumber() {
        return merchantReferenceNumber;
    }

    /**
     * @param txnRefNum The merchant's Transaction Reference Number to set
     */
    public void setReferenceNumber(String txnRefNum) {
        this.merchantReferenceNumber = txnRefNum;
    }

    /**
     * @return The merchant's Transaction Name
     */
    public String getTransactionName() {
        return merchantTransactionName;
    }

    /**
     * @param txnName The merchant's Transaction Name to set
     */
    public void setTransactionName(String txnName) {
        this.merchantTransactionName = txnName;
    }

    /**
     * @return The GCash mobile number
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber The GCash mobile number to set
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return The GCash PIN
     */
    public String getPin() {
        return pin;
    }

    /**
     * @param pin The GCash PIN to set
     */
    public void setPin(String pin) {
        this.pin = pin;
    }

    /**
     * @return The amount of GCash transaction
     */
    public float getAmount() {
        return amount;
    }

    /**
     * @param amount The amount of GCash transaction to set
     */
    public void setAmount(float amount) {
        this.amount = amount;
    }
}
