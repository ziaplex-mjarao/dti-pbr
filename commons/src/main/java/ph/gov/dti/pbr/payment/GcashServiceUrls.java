/*
 *  GCash Payment Client - <one line to give a brief idea of what it does.>
 *  Copyright (C) 2016 Department of Trade and Industry.
 */
package ph.gov.dti.pbr.payment;

/**
 *
 * @author Marlon Janssen Arao
 * @since 5 October 2015
 * @version 1.0.0
 */
public class GcashServiceUrls {
    private static final String COLLECTSERVICEURL = 
        "https://journey.globe.com.ph/.testing/rs/index.php/api/collect/sell";
    private static final String INQUIRELATESTURL =
        "https://journey.globe.com.ph/.testing/rs/index.php/api/collect/inquire_latest";
    private static final String INQUIREALLURL =
        "https://journey.globe.com.ph/.testing/rs/index.php/api/collect/inquire_all";
    private static final String COUNTTRANSACTIONSURL =
        "https://journey.globe.com.ph/.testing/rs/index.php/api/collect/count_transaction";

    
    private GcashServiceUrls() {}
    
    
    private static class GcashServiceUrlsManagerHolder {
        private final static GcashServiceUrls INSTANCE = new GcashServiceUrls();
    }


    /**
     * @return The GCash "Collect" web service URL
     */
    public static String getCollectServiceUrl() { return COLLECTSERVICEURL; }

    /**
     * @return The GCash "Inquire Latest" web service URL
     */
    public static String getInquireLatestUrl() { return INQUIRELATESTURL; }

    /**
     * @return The GCash "Inquire All" web service URL
     */
    public static String getInquireAllUrl() { return INQUIREALLURL; }

    /**
     * @return The GCash "Count Transactions" web service URL
     */
    public static String getCountTransactionsUrl() { return COUNTTRANSACTIONSURL; }
    
    public static GcashServiceUrls getInstance() {
        return GcashServiceUrlsManagerHolder.INSTANCE;
    }
}
