/*
 *  Commons String Utility - <one line to give a brief idea of what it does.>
 *  Copyright (C) 2016 Department of Trade and Industry.
 */
package ph.gov.dti.pbr.commons.stringutils;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author Marlon Janssen Arao
 * @since 9 October 2016
 * @version 1.0.0
 */
public class BasicToken {
    private static class BasicTokenManagerHolder {
        private final static BasicToken INSTANCE = new BasicToken();
    }
    
    private BasicToken() {}
    
    public static BasicToken getInstance() {
        return BasicTokenManagerHolder.INSTANCE;
    }
    
    /**
     * 
     * @param trnLength the length of the Transaction Reference Number
     * @param alphaCount the length of the alphabetic characters in the token
     * @param numericCount the length of the remaining numeric characters of the token
     * @return the token as formatted by the parameters
     */
    public static List<String> generate(int trnLength, int alphaCount, int numericCount) {
        if(trnLength < 8) { trnLength = 8; }
        if(alphaCount < 3) { alphaCount = 3; }
        if(numericCount < 5) { numericCount = 5; }

        List<String> token = new ArrayList();
        String trn = RandomStringUtils.randomNumeric(trnLength);
        String alpha = RandomStringUtils.random(alphaCount, 
                    "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        String numeric = RandomStringUtils.randomNumeric(numericCount);
        int tokenLength = trnLength + alphaCount + numericCount;
        StringBuilder tokenized = new StringBuilder(tokenLength);

        tokenized.append(alpha);
        tokenized.append(numeric);
        tokenized.append(trn);
        
        token.add(trn);
        
        token.add(tokenized.toString());

        return token;
    }
}
