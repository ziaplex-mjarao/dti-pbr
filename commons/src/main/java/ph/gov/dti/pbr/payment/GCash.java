/*
 *  GCash Payment Client - <one line to give a brief idea of what it does.>
 *  Copyright (C) 2016 Deparment of Trade and Industry.
 */
package ph.gov.dti.pbr.payment;

/**
 *
 * @author Marlon Janssen Arao
 * @since 5 October 2015
 * @version 1.0.0
 */
public class GCash {
    private static class GCashManagerHolder {
        private final static GCash INSTANCE = new GCash();
    }
    
    private GCash() { }
    
    public static GCash getInstance() {
        return GCashManagerHolder.INSTANCE;
    }
    
    /**
     *
     * @param txnInfo the transaction information to process
     */
    public static void collect(GcashTxnInfos txnInfo) { }
    
    /**
     * 
     */
    public static void countTransactions() { }
    
    /**
     * 
     */
    public static void inquireAll() { }
    
    /**
     * 
     */
    public static void inquireLatest() { }
}
