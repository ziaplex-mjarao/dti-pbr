/*
 * Copyright (C) 2016 Department of Trade and Industry.
 */
package ph.gov.dti.pbr.commons.stringutils;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author Marlon Janssen Arao
 * @since 7 October 2016
 * @version 1.0.0
 */
public class BasicTokenTest {
    private static int trnLength;
    private static int alphaCount;
    private static int numericCount;
    private static int expResult;
    private static int result;
    private static List<String> token;

    public BasicTokenTest() { }
      
    @BeforeClass
    public static void setUpClass() {
        trnLength = 8;
        alphaCount = 3;
        numericCount = 5;
        token = BasicToken.generate(trnLength, alphaCount, numericCount);
    }
    
    @Test
    public void testTokenAlpha() {
        System.out.println("testing BasicTokenTest... alpha length & validation tests");
        expResult = alphaCount;
        result = 0;
        System.out.println("......................... generate tokens!");
        System.out.print("........................... TRN >> ");
        System.out.print(token.get(0) + "\n");
        System.out.print("........................... token >> ");
        System.out.print(token.get(1) + ", " + "\n");
        System.out.print("............................ expected length:");
        System.out.print(expResult + "\n");
        System.out.print("............................ alphabetic in token: ");
        System.out.print(token.get(1).substring(0, expResult) + "\n");
        System.out.print("............................ real alphabetic length:");
        result = token.get(1).substring(0, alphaCount).length();
        System.out.print(result + "\n");
        assertEquals(token.get(1).substring(0, alphaCount).length(), expResult);
        assertTrue(token.get(1).substring(0, alphaCount).chars().allMatch(Character::isLetter));
    }
    
    @Test
    public void testTokenNumeric() {
        System.out.println("testing BasicTokenTest... numeric length & validation tests");
        expResult = numericCount;
        result = 0;
        System.out.println("......................... generate tokens!");
        System.out.print("........................... TRN >> ");
        System.out.print(token.get(0) + "\n");
        System.out.print("........................... token >> ");
        System.out.print(token.get(1) + ", " + "\n");
        System.out.print("............................ expected length:");
        System.out.print(expResult + "\n");
        System.out.print("............................ numeric in token: ");
        System.out.print(token.get(1).substring(alphaCount, alphaCount + expResult) + "\n");
        System.out.print("............................ real alphabetic length:");
        result = token.get(1).substring(alphaCount, alphaCount + expResult).length();
        System.out.print(result + "\n");
        assertEquals(token.get(1).substring(alphaCount, alphaCount + expResult).length(), expResult);
        assertTrue(token.get(1).substring(alphaCount, alphaCount + expResult).chars().allMatch(x -> Character.isDigit(x)));
    }
    
    @Test
    public void testTokenLength() {
        System.out.println("testing BasicTokenTest... token length test");
        expResult = 0;
        result = 0;
        System.out.println("......................... generate tokens!");
        System.out.print("........................... TRN >> ");
        System.out.print(token.get(0) + "\n");
        System.out.print("........................... token >> ");
        System.out.print(token.get(1) + ", " + "\n");
        System.out.print("............................ expected length:");
        expResult = alphaCount + numericCount + trnLength;
        System.out.print(expResult + "\n");
        System.out.print("............................ real token length:");
        result = token.get(1).length();
        System.out.print(result + "\n");
        assertEquals(result, expResult);
    }
}
