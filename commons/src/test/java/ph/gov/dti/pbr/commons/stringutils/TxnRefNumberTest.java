/*
 * Copyright (C) 2016 Department of Trade and Industry
 */
package ph.gov.dti.pbr.commons.stringutils;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Marlon Janssen Arao
 * @since 7 October 2016
 * @version 1.0.0
 */
public class TxnRefNumberTest {
    private static int trnLength;
    private static String trn;
    private static int expResult;
    private static int result;
    
    public TxnRefNumberTest() { }
    
    @Before
    public void setUp() {
        trnLength = 8;
        trn = TxnRefNumber.generate(trnLength);
    }
    
    @Test
    public void testTrn() {
        expResult = trnLength;
        result = trn.length();
        System.out.println("testing TxnRefNumberTest... transaction code tests");
        System.out.println("........................... generate TRN!");
        System.out.print("........................... expected length:");
        System.out.print(expResult + "\n");
        System.out.print("........................... real length:");
        System.out.print(result + "\n");
        assertEquals(trn.length(), trnLength);
    }
}
